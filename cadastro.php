<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>Destino Sustentável</title>
<link rel="shortcut icon" href="images/icone.ico">
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Employee Application Form Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript">
addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); }
</script>
<!-- //for-mobile-apps -->
<link href="css/style_cadastro.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="css/jquery-ui.css" />
<script src="js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="js/jquery.mask.min.js"></script>
<script>
    jQuery(function($){
        $('.cep').mask('00000-000', {reverse: true});
        $('.cpf').mask('000.000.000-00', {reverse: true});
        $('.CNPJ').mask('00.000.000/0000-00', {reverse: true});
        $('.telefone').mask('00 0000-0000', {reverse: true});
        $('.celular').mask('00 00000-0000', {reverse: true});
    });
</script>
<link href="//fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap" rel="stylesheet">
<style>
    .invisivel{
        display: none;
    }
    .visivel{
        visibility: visible;
    }
    .menu-header {
		width: 100%;
		height: 35px;
		padding: 15px 0 15px 0;
		font-size: 16px;
		text-transform: uppercase;
		font-family: 'Source Sans Pro', sans-serif !important;
		text-align: right;
	}
	.menu-header li a {
		color: #fff;
		float: right;
		padding: 5px 12px 17px;
		margin: 0 10px 0 0;
	}
	.menu-list a:hover {
		color: #007b36;
		border-radius: 5px;
		transition: .8s;
	}
</style>
</head>
<body>
    <!-- MENU -->
    <div class="menu-header">
        <ul class="menu-list">
            <li><a href="http://www.destinosustentavel.org/wp-content/themes/ds/login/index.php">Login</a></li>
            <li><a href="http://www.destinosustentavel.org/">Início</a></li>
        </ul>
    </div>
    <!-- //MENU -->
	<div class="main">
		<img src="images/logo.png" alt=" ">
		<h1>Cadastro</h1>
		<div class="w3_agile_main_grids">
			<div class="agileits_w3layouts_main_grid">

				<form name="cadastro" id="upload" action="c_cadastro.php" method="post" enctype="multipart/form-data">
					<div class="agile_main_grid_left">
						<div class="w3_agileits_main_grid_left_grid">
                            <h3>Idenficação:</h3>
                            <div class="agileits_main_grid_left_l_grids">
                                <div class="w3_agileits_main_grid_left_l">
                                    <h2>Tipo de Pessoa: <span class="wthree_color">*</span></h2>
                                </div>
                                <div class="w3_agileits_main_grid_left_r">
                                    <div class="agileinfo_radio_button">
                                        <label class="radio"><input type="radio" name="radio" value="F" onchange="AtivCampFisica()"><i></i>Física</label>
                                    </div>
                                    <div class="agileinfo_radio_button">
                                        <label class="radio"><input type="radio" name="radio" value="J" onchange="AtivCampJuridica()"><i></i>Jurídica</label>
                                    </div>
                                    <div class="clear"> </div>
                                </div>
                                <div class="clear"> </div>
						     </div>

                            <div id="p_fisica" class="invisivel">
                                <select name="tipo_user" id="w3_agileits_select" class="w3layouts_select" onchange="change_country(this.value)">
                                    <option value="" disabled selected>Tipo de Usuário: <span class="wthree_color">*</span></option>
                                    <option value="ar">Artesão</option>
                                    <option value="ca">Catador</option>
                                    <option value="co">Colaborador</option>
                                </select>
                                <input type="text" name="nome" placeholder="Nome *">
                                <p>Data de Nascimento: <span class="wthree_color">*</span></p>
                                <input type="date" name="data" class="dataNascimento" placeholder="Data de Nascimento *" maxlength="8">
                            </div>
                            <div id="p_juridica" class="invisivel">
                                <select name="jtipo_user" id="w3_agileits_select" class="w3layouts_select" onchange="change_country(this.value)">
                                    <option value="" disabled selected>Tipo de Usuário <span class="wthree_color">*</span></option>
                                    <option value="as">Associação de Catadores</option>
                                    <option value="co">Cooperativa de Reciclagem</option>
                                    <option value="em">Empresa</option>
				    <option value="is">Instituição de Ensino</option>
                                    <option value="in">Indústria</option>
                                </select>
                                <input type="text" name="nome_fantasia" placeholder="Nome Fantasia *" >
                                <input type="text" name="nome_representante" placeholder="Nome do Representante *" >
                                <input type="text" name="cpf" id="cpf" class="cpf" placeholder="CPF *" >
                                <h3 id="respCPF" class="invisivel"></h3>
                                <input type="text" name="cnpj" id="cnpj" class="CNPJ" placeholder="CNPJ *" >
                                <h3 id="respCNPJ" class="invisivel"></h3>
                            </div>
						    <input type="text" name="nomeUsuario" id="nomeUsuario" placeholder="Nome de usuário *" required="">
                            <h3 id="respNomeUsuario" class="invisivel"></h3>
							<input type="password" name="Senha" id="senha1" onkeypress="amplitudeSenha()" placeholder="Senha *" required="">
                            <div id="statusSenha">
                            </div>
							<input type="password" name="ConfirmaSenha" id="senha2" onchange="comparaSenhas()" placeholder="Confirmar Senha *" required="">
                            <div id="sttComparaSenha">
                            </div>
						</div>
                        <div class="w3layouts_file_upload">
                                <h3>Imagem:</h3>
                                <div class="w3_agileinfo_file">
                                    <input type="file" id="fileselect" name="imagem" multiple="imagemmultiple" />
                                    <h4>OR</h4>
                                    <div id="filedrag">Arraste e solte a imagem aqui.</div>
                                </div>
                        </div>
					</div>
					<div class="agile_main_grid_left">
                        <h3>Endereço:</h3>
						<input type="text" name="cep" class="cep" placeholder="CEP *" required="">
                        <h3>Contato:</h3>
                        <input type="text" class="celular" name="celular" placeholder="Celular">
                        <input type="text" class="telefone" name="telefone" placeholder="Telefone">
                        <input type="email" name="email" id="email" placeholder="Email *" required="">
                        <h3 id='respostaEmail' class="invisivel"></h3>
                        <input type="email" name="email2" onchange="comparaEmail()" placeholder="Confirmar Email *" required="">
                        <div id="sttComparaEmail">
                        </div>
					</div>
					<div class="clear"> </div>
                    <div id="btnCadastro">
                        <input type="submit" value="Cadastrar" onclick="return verifTipoPessoa();">
                        <a href="http://www.destinosustentavel.org/"><input type="button" value="Voltar"></a>
                    </div>
				</form>
			</div>
		</div>
		<div class="agileits_copyright">
			<p>© 2020 Destino Sustentável. All rights reserved | Design by <a href="http://w3layouts.com/" target="_blank">W3layouts</a></p>
		</div>
	</div>

	<script src="js/filedrag.js"></script>

	<!-- start-date-piker-->
    <script src="js/jquery-ui.js"></script>
    <script>
        $(function() {
            $( "#datepicker" ).datepicker();
        });
    </script>
	<!-- //End-date-piker -->

    <script type="application/javascript">
    function AtivCampFisica(){
        document.getElementById("p_juridica").className = "invisivel";
        document.getElementById("p_fisica").className = "visivel";
        document.cadastro.celular.placeholder = "Celular *";
        document.cadastro.telefone.placeholder = "Telefone";
    }

    function AtivCampJuridica() {
        document.getElementById("p_fisica").className = "invisivel";
        document.getElementById("p_juridica").className = "visivel";
        document.cadastro.telefone.placeholder = "Telefone *";
        document.cadastro.celular.placeholder = "Celular";
    }

    function amplitudeSenha() {
        senha = document.cadastro.senha1.value;
        if(senha.length<=3){
            document.getElementById("statusSenha").innerHTML = '<h3 id="txtStatusSenha">A senha é fraca!</h3>';
            document.getElementById("txtStatusSenha").style.color = "#F9001D";
        }else if(senha.length<=6){
            document.getElementById("statusSenha").innerHTML = '<h3 id="txtStatusSenha">A senha é média!</h3>';
            document.getElementById("txtStatusSenha").style.color = "#FFE926";
        }else if(senha.length>6){
           document.getElementById("statusSenha").innerHTML = '<h3 id="txtStatusSenha">A senha é forte!</h3>';
           document.getElementById("txtStatusSenha").style.color = "#0088E2";
        }
    }

    function comparaSenhas() {
        senha1 = document.cadastro.senha1.value;
        senha2 = document.cadastro.senha2.value;

        if(senha1 != senha2) {
            document.getElementById("sttComparaSenha").innerHTML = '<h3 id="txtComparaSenha">As senhas são diferentes!</h3>';
            document.getElementById("txtComparaSenha").style.color = "#F9001D";
        } else {
            document.getElementById("sttComparaSenha").innerHTML = '<h3 id="txtComparaSenha">As senhas são iguais!</h3>';
            document.getElementById("txtComparaSenha").style.color = "#0088E2";
        }
    }

    function comparaEmail() {
        email1 = document.cadastro.email.value;
        email2 = document.cadastro.email2.value;

        if(email1 != email2) {
            document.getElementById("sttComparaEmail").innerHTML = '<h3 id="txtComparaEmail">Os Emails são diferentes!</h3>';
            document.getElementById("txtComparaEmail").style.color = "#F9001D";
        } else {
            document.getElementById("sttComparaEmail").innerHTML = '<h3 id="txtComparaEmail">Os Emails são iguais!</h3>';
            document.getElementById("txtComparaEmail").style.color = "#0088E2";
        }
    }

    function verifTipoPessoa() {
        valor1 = document.cadastro.radio[0].checked;
        valor2 = document.cadastro.radio[1].checked;

        if((valor1 == false) && (valor2 == false)){
            alert("Selecione qual o seu tipo de pessoa!\n Pessoa Física ou Jurídica.");
            document.cadastro.radio[0].focus();
            return false;
        } else {
            if(valor1 == true) {
                if(document.cadastro.tipo_user.value=="") {
                    alert("Para continuar, selecione o tipo de usuário!");
                    document.cadastro.tipo_user.focus();
                    return false;
                } else if(document.cadastro.nome.value == "") {
                    alert("Para continuar, insira um Nome!");
                    document.cadastro.nome.focus();
                    return false;
                } lse if(document.cadastro.data.value == "") {
                    alert("Para continuar, insira a data de Nascimento!");
                    document.cadastro.data.focus();
                    return false;
                } else if(document.cadastro.celular.value == "") {
                    alert("Para continuar, insira um número de Celular!");
                    document.cadastro.celular.focus();
                    return false;
                } else if(document.cadastro.senha1.value == ""){
                    alert("Para continuar, insira uma senha!");
                    document.cadastro.senha1.focus();
                    return false;
                } else if(document.cadastro.senha2.value == ""){
                    alert("Confirme a senha para verificar a validade da mesma!");
                    document.cadastro.senha2.focus();
                    return false;
                } else if(document.getElementById("respostaEmail").firstChild.nodeValue =="Existe um usuário com este email!"){
                    alert("O email já existe, tente outro!");
                    document.cadastro.email.focus();
                    return false;
                } else if(document.getElementById("respNomeUsuario").firstChild.nodeValue =="Existe um usuário com este nome!"){
                    alert("O nome de usuário já existe, tente outro!");
                    document.cadastro.email.focus();
                    return false;
                } else if(document.cadastro.email.value != document.cadastro.email2.value){
                    alert("Os email's são diferentes, tente novamente!");
                    document.cadastro.email.focus();
                    return false;
                } else if(document.cadastro.senha.value != document.cadastro.senha2.value){
                    alert("As senhas são diferentes, tente novamente!");
                    document.cadastro.senha.focus();
                    return false;
                } else{
                    return true;
                }
            } else if(valor2 == true) {
                if(document.cadastro.jtipo_user.value=="") {
                    alert("Para continuar, selecione o tipo de usuário!");
                    document.cadastro.jtipo_user.focus();
                    return false;
                } else if(document.cadastro.nome_fantasia.value == "") {
                    alert("Para continuar, insira um Nome Fantasia!");
                    document.cadastro.nome_fantasia.focus();
                    return false;
                } else if(document.cadastro.nome_representante.value == "") {
                    alert("Para continuar, insira o Nome do Representante!");
                    document.cadastro.nome_representante.focus();
                    return false;
                } else if(document.cadastro.cpf.value == "") {
                    alert("Para continuar, insira o CPF do representante!");
                    document.cadastro.cpf.focus();
                    return false;
                } else if(document.cadastro.cnpj.value == "") {
                    alert("Para continuar, insira o CNPJ!");
                    document.cadastro.cnpj.focus();
                    return false;
                } else if(document.cadastro.telefone.value == "") {
                    alert("Para continuar, insira um número de Telefone!");
                    document.cadastro.telefone.focus();
                    return false;
                } else if(document.cadastro.senha1.value == "") {
                    alert("Para continuar, insira uma senha!");
                    document.cadastro.senha1.focus();
                    return false;
                } else if(document.cadastro.senha2.value == "") {
                    alert("Confirme a senha para verificar a validade da mesma!");
                    document.cadastro.senha2.focus();
                    return false;
                } else if(document.getElementById("respostaEmail").firstChild.nodeValue =="Existe um usuário com este email!") {
                    alert("O email já existe, tente outro!");
                    document.cadastro.email.focus();
                    return false;
                } else if(document.getElementById("respNomeUsuario").firstChild.nodeValue =="Existe um usuário com este nome!") {
                    alert("O nome de usuário já existe, tente outro!");
                    document.cadastro.email.focus();
                    return false;
                } else if(document.getElementById("respCPF").firstChild.nodeValue =="CPF já cadastrado no sistema!") {
                    alert("O CPF já está cadastrado no Sistema!");
                    document.cadastro.cpf.focus();
                    return false;
                } else if(document.getElementById("respCNPJ").firstChild.nodeValue =="CNPJ já cadastrado no sistema!") {
                    alert("O CNPJ já está cadastrado no Sistema!");
                    document.cadastro.email.focus();
                    return false;
                } else if(document.cadastro.email.value != document.cadastro.email2.value) {
                    alert("Os email's são diferentes, tente novamente!");
                    document.cadastro.email.focus();
                    return false;
                } else if(document.cadastro.senha.value != document.cadastro.senha2.value) {
                    alert("As senhas são diferentes, tente novamente!");
                    document.cadastro.senha.focus();
                    return false;
                } else {
                    return true;
                }
            }
        }
    }
    </script>

    <script language="javascript">
        let email = $("#email");
        email.blur(function() {
            $.ajax({
				url: 'verificaEmail.php',
				type: 'POST',
				data:{"email" : email.val()},
				success: function(data) {
				    console.log(data);
				    data = $.parseJSON(data);
				    $("#respostaEmail").text(data.email);
                    AtribuiCorEmail();
                }
            });
        });

        //cor para email
        function AtribuiCorEmail() {
            let resposta = document.getElementById("respostaEmail").firstChild.nodeValue;
            document.getElementById("respostaEmail").className = "visivel";
            if(resposta == "Existe um usuário com este email!")
                document.getElementById("respostaEmail").style.color = "#F9001D";
            else if(resposta == "Este Email está disponível!")
                document.getElementById("respostaEmail").style.color = "#0088E2";
        }

        let nomeUsuario = $("#nomeUsuario");
        nomeUsuario.blur(function() {
            $.ajax({
				url: 'verificaNomeUsuario.php',
				type: 'POST',
				data:{"nomeUsuario" : nomeUsuario.val()},
				success: function(data) {
				    console.log(data);
				    data = $.parseJSON(data);
				    $("#respNomeUsuario").text(data.nomeUsuario);
                    AtribuiCorNomUsuario();
                }
            });
        });

        //cor para email
        function AtribuiCorNomUsuario() {
            let resposta = document.getElementById("respNomeUsuario").firstChild.nodeValue;
            document.getElementById("respNomeUsuario").className = "visivel";
            if(resposta == "Existe um usuário com este nome!")
                document.getElementById("respNomeUsuario").style.color = "#F9001D";
            else if(resposta == "Este Nome está disponível!")
                document.getElementById("respNomeUsuario").style.color = "#0088E2";
        }

        let cpf = $("#cpf");
        cpf.blur(function() {
            $.ajax({
				url: 'verificaCPF.php',
				type: 'POST',
				data:{"cpf" : cpf.val()},
				success: function(data) {
				    console.log(data);
				    data = $.parseJSON(data);
				    $("#respCPF").text(data.cpf);
                    AtribuiCorCPF();
                }
            });
        });

        //cor para email
        function AtribuiCorCPF(){
            let resposta = document.getElementById("respCPF").firstChild.nodeValue;
            document.getElementById("respCPF").className = "visivel";
            if(resposta == "CPF já cadastrado no sistema!")
                document.getElementById("respCPF").style.color = "#F9001D";
            else if(resposta == "CPF disponível!")
                document.getElementById("respCPF").style.color = "#0088E2";
        }

        let nomeUsuario = $("#nomeUsuario");
        nomeUsuario.blur(function() {
            $.ajax({
				url: 'verificaNomeUsuario.php',
				type: 'POST',
				data:{"nomeUsuario" : nomeUsuario.val()},
				success: function(data) {
				    console.log(data);
				    data = $.parseJSON(data);
				    $("#respNomeUsuario").text(data.nomeUsuario);
                    AtribuiCorNomUsuario();
                }
            });
        });

        //cor para email
        function AtribuiCorNomUsuario(){
            let resposta = document.getElementById("respNomeUsuario").firstChild.nodeValue;
            document.getElementById("respNomeUsuario").className = "visivel";
            if(resposta == "Existe um usuário com este nome!")
                document.getElementById("respNomeUsuario").style.color = "#F9001D";
            else if(resposta == "Este Nome está disponível!")
                document.getElementById("respNomeUsuario").style.color = "#0088E2";
        }

        let cnpj = $("#cnpj");
        cnpj.blur(function() {
            $.ajax({
				url: 'verificaCNPJ.php',
				type: 'POST',
				data:{"cnpj" : cnpj.val()},
				success: function(data) {
				    console.log(data);
				    data = $.parseJSON(data);
				    $("#respCNPJ").text(data.cnpj);
                    AtribuiCorCNPJ();
                }
            });
        });
        
        //cor para email
        function AtribuiCorCNPJ(){
            let resposta = document.getElementById("respCNPJ").firstChild.nodeValue;
            document.getElementById("respCNPJ").className = "visivel";
            if(resposta == "CNPJ já cadastrado no sistema!")
                document.getElementById("respCNPJ").style.color = "#F9001D";
            else if(resposta == "CNPJ disponível!")
                document.getElementById("respCNPJ").style.color = "#0088E2";
        }
    </script>
    
    <script type="text/javascript" src="js/pace.min.js"></script>
</body>
</html>
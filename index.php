<!DOCTYPE html>
<html lang="zxx">
<head>
	<title>Destino Sustentável</title>
	<!-- for-mobile-apps -->
	<link rel="shortcut icon" href="<?php bloginfo('template_url');?>/images/logo.png">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Reinforce Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!-- //for-mobile-apps -->
	<link href="<?php bloginfo('template_url');?>/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<!--gallery -->
	<link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url');?>/css/cm-overlay.css" />
	<!-- //gallery -->
	<link href="<?php bloginfo('template_url');?>/css/font-awesome.css" rel="stylesheet" media="all">
	<link href="<?php bloginfo('template_url');?>/css/owl.carousel.css" rel="stylesheet" type="text/css" media="all">
	<link href="<?php bloginfo('template_url');?>/css/owl.theme.css" rel="stylesheet" type="text/css" media="all">
	<link href="<?php bloginfo('template_url');?>/css/style.css" rel="stylesheet" type="text/css" media="all">
	<link href="<?php bloginfo('template_url');?>/css/header.css" rel="stylesheet" type="text/css" media="all">
	<link href="<?php bloginfo('template_url');?>/css/timeline.css" rel="stylesheet" type="text/css" media="all">
	<!-- Google fonts -->
	<link href="//fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Cabin:wght@400;700&family=Passion+One:wght@400;700&display=swap" rel="stylesheet">
	<!-- //Google fonts -->
	<style>
		/* Galeria */
		.modula-items {
			padding: 0;
			display: grid;
			grid-template-columns: repeat(auto-fit, minmax(325px, 1fr));
			justify-items: center;
		}

		/* Informações */
		.service-grids:not(#materias) {
			height: 320px;
		}

		.buttons-container {
			display: flex;
			justify-content: center;
			margin-top: 20px;
		}

		.buttons-container .boxBotao {
			margin: 25px 15px 0 15px;
		}

		@media(max-width: 1199px) {
			.service-grids:not(#materias) {
				height: 360px;
			}

			.service-grids:not(#materias) .services-text {
				height: 205px;
			}
		}

		@media(max-width: 991px) {
			.service-grids:not(#materias) {
				height: 250px;
			}

			.service-grids:not(#materias) .services-text {
				height: 140px;
			}
		}

		@media(max-width: 767px) {
			.service-grids:not(#materias) {
				height: 220px;
			}

			.service-grids:not(#materias) .services-text {
				height: 115px;
			}
		}

		@media(max-width: 736px) {
			.service-grids:not(#materias) {
				height: 250px;
			}

			.service-grids:not(#materias) .services-text {
				height: 140px;
			}
		}

		@media(max-width: 696px) {
			.service-grids:not(#materias) {
				height: 265px;
			}

			.service-grids:not(#materias) .services-text {
				height: 155px;
			}
		}

		@media(max-width: 550px) {
			.service-grids:not(#materias) {
				height: 270px;
			}

			.service-grids:not(#materias) .services-text {
				height: 185px;
			}
		}

		@media(max-width: 507px) {
			.service-grids:not(#materias) {
				height: 290px;
			}

			.service-grids:not(#materias) .services-text {
				height: 205px;
			}
		}

		@media(max-width: 480px) {
			.service-grids:not(#materias) {
				height: 220px;
			}

			.service-grids:not(#materias) .services-text {
				height: 140px;
			}
		}

		@media(max-width: 447px) {
			.buttons-container {
				flex-direction: column;
			}
		}

		@media(max-width: 363px) {
			.service-grids:not(#materias) {
				height: 230px;
			}

			.service-grids:not(#materias) .services-text {
				height: 155px;
			}
		}

		/* Matérias */
		.images-posts img {
			width: 100%;
		}

		@media(max-width: 1200px) {
			.post-resume {
				height: 300px !important;
			}
		}

		@media(max-width: 991px) {
			.post-resume {
				height: 160px !important;
			}
		}

		@media(max-width: 538px) {
			.post-resume {
				height: 180px !important;
			}
		}

		@media(max-width: 405px) {
			.post-resume {
				height: 200px !important;
			}
		}

		@media(max-width: 368px) {
			.post-resume {
				height: 220px !important;
			}
		}

		@media(max-width: 320px) {
			.post-resume {
				height: 245px !important;
			}
		}
	</style>
</head>

<?php
	//include "conexao.php";
	//session_start();
?>

<body>
	<?php include "header.php"; ?>

	<!-- Sobre o projeto -->
	<div class="section-w3ls about-w3ls" id="about">
		<h3 class="title-agile about text-center">Sobre o projeto</h3>
		<span class="about-p text-center">
			<span class="sub-title"></span>Não é lixo, é matéria prima
			<span class="sub-title"></span></span>
			<div class="container">
			<div class="about-main about1">
				<div class="col-md-4 about-grid-w3ls">
					<div class="about-sub-grid-w3ls">
						<h4>Abrangência</h4>
						<p>O Destino Sustentável possibilita a interação entre catadores, associações, cooperativas de reciclagem, indústrias, empresas, artesãos e colaboradores.</p>
					</div>
					<div class="about-sub-grid-w3ls">
						<h4>Surgimento</h4>
						<p>O projeto iniciou em 2015, por uma iniciativa de estudantes de computação da Universidade Federal do Pará.</p>
					</div>
					<div class="about-sub-grid-w3ls">
                        <h4>Missão</h4>
						<p>Possibilitar a integração das pessoas que fazem parte do ciclo da reciclagem, para que aconteça a destinação adequada dos resíduos sólidos srecicláveis de forma efetiva nos municípios sem saneamento básico e coleta seletiva.</p>
					</div>
				</div>
				<div class="col-md-4 about-grid-w3ls1">
          <br/><br/><br/><br/><br/><br/><br/><br/><br/>
					<img src="<?php bloginfo('template_url');?>/images/logo.png" class="img-responsive" />
				</div>
				<div class="col-md-4 about-grid-w3ls ab-right">
					<div class="about-sub-grid-w3ls">
						<h4>Sem fins lucrativos</h4>
						<p>Desde o início, o projeto segue sem fins lucrativos e sem financiamento. O trabalho dos desenvolvedores é totalmente voluntário.</p>
					</div>
					<div class="about-sub-grid-w3ls">
            <h4>Visão</h4>
						<p>Ser um meio de integração dos agentes responsáveis pela reciclagem em prol da destinação adequada dos resíduos sólidos recicláveis.</p>
					</div>
					<div class="about-sub-grid-w3ls">
						<h4>Valores</h4>
						<p>Diversidade de ideias e opiniões.<br/>
						Engajamento social e ambiental.<br/>
						Multiplicidade de propostas para o bem comum.<br/>
						Ações e parcerias positivas.</p>
					</div>
				</div>
				
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- //Sobre o projeto -->

	<!-- Informações -->
	<div class="services1">
	<div class="section-w3ls services-w3ls" id="services">
		<h3 class="title-agile1 about text-center">Informações</h3>
		<div class="container">
			<div class="about-main">
				<div class="col-md-3 col-sm-6  service-grids">
					<div class="services-head-w3ls" id="coisa">
						<span class="fa fa-bullhorn" aria-hidden="true"></span>
					</div>
					<div class="services-text">
						<h4>Ofertas</h4>
						<p>Faça oferta dos seus materiais recicláveis, quem quiser os materiais pode atender sua oferta.</p>
					</div>
				</div>

				<div class="col-md-3 col-sm-6  service-grids">
					<div class="services-head-w3ls">
						<span class="fa fa-pencil" aria-hidden="true"></span>
					</div>
					<div class="services-text">
						<h4>Pedidos</h4>
						<p>Realize pedidos de material reciclável, quem tive-los pode atender e fornecer o material.</p>
					</div>
				</div>

				<div class="col-md-3 col-sm-6  service-grids">
					<div class="services-head-w3ls">
						<span class="fa fa-home" aria-hidden="true"></span>
					</div>
					<div class="services-text">
						<h4>Coleta</h4>
						<p>Os catadores podem visualizar ofertas e atendê-las antes de sair de casa, indo direto ao objetivo.</p>
					</div>
				</div>

				<div class="col-md-3 col-sm-6  service-grids">
					<div class="services-head-w3ls">
						<span class="fa fa-group" aria-hidden="true"></span>
					</div>
					<div class="services-text">
						<h4>Comunidades</h4>
						<p>Pessoas de localidades próximas podem fazer ofertas coletivas de material.</p>
					</div>
				</div>

				<div class="col-md-3 col-sm-6  service-grids">
					<div class="services-head-w3ls">
						<span class="fa fa-recycle" aria-hidden="true"></span>
					</div>
					<div class="services-text">
						<h4>Doação / Comercialização</h4>
						<p>O Destino Sustentável não interfere no andamento das negociações ou doações de material reciclável. Fica a critério do usuário.</p>
					</div>
				</div>

				<div class="col-md-3 col-sm-6  service-grids">
					<div class="services-head-w3ls">
						<span class="fa fa-line-chart" aria-hidden="true"></span>
					</div>
					<div class="services-text">
						<h4>Estatísticas</h4>
						<p>Visualize estatisticamente a demanda e quantidade disponível de cada tipo de material de acordo com o estado/região.</p>
					</div>
				</div>

				<div class="col-md-3 col-sm-6  service-grids">
					<div class="services-head-w3ls">
						<span class="fa fa-star-o" aria-hidden="true"></span>
					</div>
					<div class="services-text">
						<h4>Ranking</h4>
						<p>A cada oferta e pedido concluídos você pode acumular pontos e subir no ranking de acordo com sua categoria.</p>
					</div>
				</div>

				<div class="col-md-3 col-sm-6  service-grids">
					<div class="services-head-w3ls">
						<span class="fa fa-calendar" aria-hidden="true"></span>
					</div>
					<div class="services-text">
						<h4>Palestras</h4>
						<p>A equipe de desenvolvimento do projeto participa de conferências e eventos por todo o Brasil. Entre em contato conosco para agendar.</p>
					</div>
				</div>

				<div class="clearfix"></div>

				<div class="buttons-container">
					<div class="boxBotao">
						<a class="botaoMais" href="<?php $base_url; ?>/index.php/manuais-tecnicos">Manuais Técnicos</a><br>
					</div>
					<div class="boxBotao">
						<a class="botaoMais" href="<?php $base_url; ?>/index.php/portal-transparencia">Portal de Transparência</a><br>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	<!-- //Informações -->

  <!-- Matérias  -->
  <?php $quant = 0; ?>
	<div class="section-w3ls services-w3ls" id="blog">
		<h3 class="title-agile about text-center">Matérias</h3>
		<div class="container">
			<div class="about-main">

 				<?php if ( have_posts() ){
					while ( have_posts() ) {
						the_post(); ?>
						<?php $quant++; ?>
						<div class="col-md-3 col-sm-6  service-grids" id="materias">
							<div class="images-posts">
								<?php if(has_post_thumbnail()): ?>
									<?php the_post_thumbnail('medium'); ?>
								<?php endif; ?>
							</div>
							<div class="services-text post-resume">
								<h4><?php the_title(); ?></h4>
								<p><?php the_excerpt(); ?></p>
								<h5><a href="<?php the_permalink(); ?>" id="continue">Continuar Lendo...</a></h5>
							</div>
						</div>
						<?php if ($quant == 4) break; ?>
					<?php } ?>
				<?php } ?>

				<div class="clearfix"></div>

				<div class="boxBotao">
					<a class="botaoMais" href="<?php $base_url; ?>/index.php/posts">Mais matérias</a><br>
				</div>
			</div>
		</div>
	</div>
  <!-- //Matérias -->

	<!-- Equipe -->
	<div class="team">
		<div id="team" class="section-w3ls team">
			<div class="container">
				<h3 class="title-agile1 about text-center">Equipe</h3>
				<div id="owl-demo-team" class="owl-carousel owl-theme">

					<div class="item">
						<div class="feedback-info">
							<div class="feedback-grids">
								<div class="feedback-img-team">
									<img src="<?php bloginfo('template_url');?>/imagens_equipe/italo.JPG" />
								</div>
								<div class="w3agile-caption">
									<h4>Ítalo Silva</h4>
									<p>Diretor Financeiro</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="feedback-info">		
							<div class="feedback-grids">
								<div class="feedback-img-team">
									<img src="<?php bloginfo('template_url');?>/imagens_equipe/luciano.jpeg" />
								</div>
								<div class="w3agile-caption">
									<h4>Luciano Teran</h4>
									<p>Diretor de T.I.</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="feedback-info">		
							<div class="feedback-grids">
								<div class="feedback-img-team">
									<img src="<?php bloginfo('template_url');?>/imagens_equipe/romario.jpg" />
								</div>
								<div class="w3agile-caption">
									<h4>Romário Silva</h4>
									<p>Diretor de Marketing</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>
					
					<div class="item">
						<div class="feedback-info">					
							<div class="feedback-grids">
								<div class="feedback-img-team">
									<img src="<?php bloginfo('template_url');?>/imagens_equipe/vitoria.jpeg" />
								</div>
								<div class="w3agile-caption">
									<h4>Vitória Rodrigues</h4>
									<p>Diretora Executiva</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="feedback-info">
							<div class="feedback-grids">
								<div class="feedback-img-team">
									<img src="<?php bloginfo('template_url');?>/imagens_equipe/adrianne.jpg" />
								</div>
								<div class="w3agile-caption">
									<h4>Adrianne Veras</h4>
									<p>Editora de vídeo</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="feedback-info">
							<div class="feedback-grids">
								<div class="feedback-img-team">
									<img src="<?php bloginfo('template_url');?>/imagens_equipe/alexandre.jpeg" />
								</div>
								<div class="w3agile-caption">
									<h4>Alexandre Oliveira</h4>
									<p>Designer</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="feedback-info">
							<div class="feedback-grids">
								<div class="feedback-img-team">
									<img src="<?php bloginfo('template_url');?>/imagens_equipe/brenda.jpeg" />
								</div>
								<div class="w3agile-caption">
									<h4>Brenda Vara</h4>
									<p>Produtora Textual</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="feedback-info">
							<div class="feedback-grids">
								<div class="feedback-img-team">
									<img src="<?php bloginfo('template_url');?>/imagens_equipe/eric.jpg" />
								</div>
								<div class="w3agile-caption">
									<h4>Cirilo Tavares</h4>
									<p>Produtor Textual</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="feedback-info">
							<div class="feedback-grids">
								<div class="feedback-img-team">
									<img src="<?php bloginfo('template_url');?>/imagens_equipe/farley.jpg" />
								</div>
								<div class="w3agile-caption">
									<h4>Farley Monteiro</h4>
									<p>Produtora Textual</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="feedback-info">
							<div class="feedback-grids">
								<div class="feedback-img-team">
									<img src="<?php bloginfo('template_url');?>/imagens_equipe/fred.jpg" />
								</div>
								<div class="w3agile-caption">
									<h4>Fred Coutinho</h4>
									<p>Desenvolvedor Web</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="feedback-info">
							<div class="feedback-grids">
								<div class="feedback-img-team">
									<img src="<?php bloginfo('template_url');?>/imagens_equipe/keven.jpg" />
								</div>
								<div class="w3agile-caption">
									<h4>Keven Carrilho</h4>
									<p>Desenvolvedor Mobile</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="feedback-info">
							<div class="feedback-grids">
								<div class="feedback-img-team">
									<img src="<?php bloginfo('template_url');?>/imagens_equipe/avila.jpg" />
								</div>
								<div class="w3agile-caption">
									<h4>Lucas Ávila</h4>
									<p>Gestor das mídias sociais</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="feedback-info">
							<div class="feedback-grids">
								<div class="feedback-img-team">
									<img src="<?php bloginfo('template_url');?>/imagens_equipe/lucas.jpg" />
								</div>
								<div class="w3agile-caption">
									<h4>Lucas Ribeiro</h4>
									<p>Analista de segurança</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="feedback-info">
							<div class="feedback-grids">
								<div class="feedback-img-team">
									<img src="<?php bloginfo('template_url');?>/imagens_equipe/markus.jpg" />
								</div>
								<div class="w3agile-caption">
									<h4>Markus Douglas</h4>
									<p>Desenvolvedor Backend</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="feedback-info">
							<div class="feedback-grids">
								<div class="feedback-img-team">
									<img src="<?php bloginfo('template_url');?>/imagens_equipe/matheus.jpg" />
								</div>
								<div class="w3agile-caption">
									<h4>Matheus Lardoza</h4>
									<p>Produtor Textual</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="feedback-info">
							<div class="feedback-grids">
								<div class="feedback-img-team">
									<img src="<?php bloginfo('template_url');?>/imagens_equipe/mayko.jpg" />
								</div>
								<div class="w3agile-caption">
									<h4>Mayko Paixão</h4>
									<p>Gestão de projetos</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="feedback-info">
							<div class="feedback-grids">
								<div class="feedback-img-team">
									<img src="<?php bloginfo('template_url');?>/imagens_equipe/tobias.jpg" />
								</div>
								<div class="w3agile-caption">
									<h4>Tobias Santos</h4>
									<p>Designer</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>
							
				</div>
			</div>
		</div>
	</div>
	<!-- //Equipe -->

  <!-- Galeria -->
	<div class="section-w3ls agileits-gallery" id="portfolio">
		<h3 class="title-agile about text-center">Galeria</h3>
		<p class="about-p text-center">Registros memoráveis sobre a trajetória do projeto</p>
		<div class="gallery-w3layouts">
			<a class="cm-overlay"></a>
			<?php echo do_shortcode( '[modula id="1417"]' ); ?>

			<div class="clearfix"></div>
			<div class="boxBotao">
				<a class="botaoMais" href="<?php $base_url; ?>/index.php/galeria">Mais fotos</a><br>
			</div>
		</div>
	</div>
  <!-- //Galeria -->

	<!-- +Voluntários -->
	<div class="feedback section-w3ls about-w3ls" id="testimonials">
		<div class="feedback-agileinfo">
			<div class="container">
				<h3 class="title-agile about text-center">+Voluntários</h3>
				<div id="owl-demo" class="owl-carousel owl-theme">
					<div class="item">
						<div class="feedback-info">
							<div class="feedback-top">
								<p> Contribuiu como desenvolvedor,<br>em 2019. </p>
							</div>
							<div class="feedback-grids">
								<div class="feedback-img">
									<img src="<?php bloginfo('template_url');?>/imagens_equipe/alisson.jpeg" />
								</div>
								<div class="feedback-img-info">
									<h5>Alissom<br>Morais</h5>
									<p>Castanhal - PA</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="feedback-info">
							<div class="feedback-top">
								<p> Contribuiu como assessora jurídica,<br>em 2020. </p>
							</div>
							<div class="feedback-grids">
								<div class="feedback-img">
									<img src="<?php bloginfo('template_url');?>/imagens_equipe/caroline.jpg" />
								</div>
								<div class="feedback-img-info">
									<h5>Ana<br>Caroline</h5>
									<p>Castanhal - PA</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="feedback-info">
							<div class="feedback-top">
								<p> Contribuiu como designer,<br>em 2019. </p>
							</div>
							<div class="feedback-grids">
								<div class="feedback-img">
									<img src="<?php bloginfo('template_url');?>/imagens_equipe/anthony.jpg" />
								</div>
								<div class="feedback-img-info">
									<h5>Anthony<br>Ribeiro</h5>
									<p>Castanhal - PA</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="feedback-info">
							<div class="feedback-top">
								<p> Contribuiu como gestora das mídias sociais,<br>de 2018 a 2020. </p>
							</div>
							<div class="feedback-grids">
								<div class="feedback-img">
									<img src="<?php bloginfo('template_url');?>/imagens_equipe/aryane.png" />
								</div>
								<div class="feedback-img-info">
									<h5>Aryane<br>Parra</h5>
									<p>Belém - PA</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="feedback-info">
							<div class="feedback-top">
								<p> Contribuiu como diretora de relações públicas, de 2019 a 2020. </p>
							</div>
							<div class="feedback-grids">
								<div class="feedback-img">
									<img src="<?php bloginfo('template_url');?>/imagens_equipe/bianca.jpeg" />
								</div>
								<div class="feedback-img-info">
									<h5>Bianca<br>Ramos</h5>
									<p>Belém - PA</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="feedback-info">
							<div class="feedback-top">
								<p> Contribuiu como produtora textual,<br>de 2018 a 2019. </p>
							</div>
							<div class="feedback-grids">
								<div class="feedback-img">
									<img src="<?php bloginfo('template_url');?>/imagens_equipe/joyce.jpg" />
								</div>
								<div class="feedback-img-info">
									<h5>Joyce<br>Mendes</h5>
									<p>Castanhal - PA</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="feedback-info">
							<div class="feedback-top">
								<p> Contribuiu como gestora das mídias sociais,<br>de 2018 a 2019. </p>
							</div>
							<div class="feedback-grids">
								<div class="feedback-img">
									<img src="<?php bloginfo('template_url');?>/imagens_equipe/luiza.jpg" />
								</div>
								<div class="feedback-img-info">
									<h5>Luiza<br>Imbelloni</h5>
									<p>Belém - PA</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="feedback-info">
							<div class="feedback-top">
								<p> Contribuiu como designer,<br>em 2019. </p>
							</div>
							<div class="feedback-grids">
								<div class="feedback-img">
									<img src="<?php bloginfo('template_url');?>/imagens_equipe/neverton.jpg" />
								</div>
								<div class="feedback-img-info">
									<h5>Neverton<br>Sousa</h5>
									<p>Castanhal - PA</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="feedback-info">
							<div class="feedback-top">
								<p> Contribuiu como gestora das mídias sociais,<br>em 2020. </p>
							</div>
							<div class="feedback-grids">
								<div class="feedback-img">
									<img src="<?php bloginfo('template_url');?>/imagens_equipe/noelly.jpg" />
								</div>
								<div class="feedback-img-info">
									<h5>Noelly<br>Lima</h5>
									<p>Castanhal - PA</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="feedback-info">
							<div class="feedback-top">
								<p> Contribuiu como diretora de marketing,<br>em 2018. </p>
							</div>
							<div class="feedback-grids">
								<div class="feedback-img">
									<img src="<?php bloginfo('template_url');?>/imagens_equipe/sara.jpg" />
								</div>
								<div class="feedback-img-info">
									<h5>Sara<br>Mercês</h5>
									<p>Castanhal - PA</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="feedback-info">
							<div class="feedback-top">
								<p> Contribuiu como coordenadora,<br>de 2015 a 2017. </p>
							</div>
							<div class="feedback-grids">
								<div class="feedback-img">
									<img src="<?php bloginfo('template_url');?>/imagens_equipe/yomara.jpg" />
								</div>
								<div class="feedback-img-info">
									<h5>Yomara<br>Pires</h5>
									<p>Castanhal - PA</p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- //+Voluntários -->

	<!-- Prêmios e Títulos -->
	<div id="awards" class="section-w3ls ">
		<div class="container">
			<h3 class="title-agile1 about text-center">Prêmios e Títulos</h3>
			<div id="owl-demo-awards" class="owl-carousel owl-theme">
				<div class="item">
					<div class="timeline-panel">
						<div class="timeline-heading">
							<div class="timeline-image">
								<img src="https://destinosustentavel.com.br/wp-content/uploads/2020/06/oficina-tracca.png">
							</div>
							<h4 class="timeline-title">II TraccaVivarte</h4>
							<p class="date"><small class="text-muted">Dezembro de 2019</small></p>
						</div>
						<div class="timeline-body">
							<p>Projeto reconhecido e premiado por suas ações em prol da sociedade e do meio ambiente, no II TraccaVivarte, realizado pela Escola Padre Salvador Traccaiolli, em Castanhal. A certificação foi estregue ao diretor financeiro Italo Silva e a diretora executiva Vitória Rodrigues.</p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="timeline-panel">
						<div class="timeline-heading">
							<div class="timeline-image">
								<img src="https://destinosustentavel.com.br/wp-content/uploads/2020/09/wtd14-1.png">
							</div>
							<h4 class="timeline-title">Concurso WTD UNICAMP</h4>
							<p class="date"><small class="text-muted">Novembro de 2019</small></p>
						</div>
						<div class="timeline-body">
							<p>Primeiro lugar no concurso Do-It-Yourself (DIY) do Workshop de Teses e Dissertações (WTD) do Instituto de Computação da Universidade de Campinas, sob representação da diretora executiva Vitória Rodrigues.</p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="timeline-panel">
						<div class="timeline-heading">
							<div class="timeline-image">
								<img src="https://destinosustentavel.com.br/wp-content/uploads/2020/09/selo-de-inovacao-1.jpg">
							</div>
							<h4 class="timeline-title">Selo de Inovação SBC</h4>
							<p class="date"><small class="text-muted">Julho de 2019</small></p>
						</div>
						<div class="timeline-body">
							<p>Primeiro lugar no concurso Selo de Inovação da Sociedade Brasileira de Computação. Ocorrido em Belém do Pará durante o XXXIX CSBC, com projeto representado pelo diretor de marketing Romário Silva, que recebeu o titulo de projeto inovador e inédito na comunidade científica.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- //Prêmios e Títulos -->

	<?php include "footer.php"; ?>

	<script src="<?php bloginfo('template_url');?>/js/jquery-2.2.3.min.js"></script>	
	<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/numscroller-1.0.js"></script>

	<!-- Slider JavaScript -->
	<script src="<?php bloginfo('template_url');?>/js/responsiveslides.min.js"></script>
	<script>
		$(function () {
			$("#slider, #slider1").responsiveSlides({
				auto: true,
				nav: false,
				speed: 1500,
				namespace: "callbacks",
				pager: true,
			});
		});
	</script>
	<!-- //Slider JavaScript -->
	
	<script>
		$(function () {
			$("#slider3").responsiveSlides({
				auto: true,
				pager: false,
				nav: true,
				speed: 500,
				namespace: "callbacks",
				before: function () {
					$('.events').append("<li>before event fired.</li>");
				},
				after: function () {
					$('.events').append("<li>after event fired.</li>");
				}
			});
		});
	</script>

	<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/numscroller-1.0.js"></script>
	
	<!-- gallery -->
	<script src="<?php bloginfo('template_url');?>/js/jquery.tools.min.js"></script>
	<script src="<?php bloginfo('template_url');?>/js/jquery.mobile.custom.min.js"></script>
	<script src="<?php bloginfo('template_url');?>/js/jquery.cm-overlay.js"></script>
	<script>
		$(document).ready(function () {
			$('.cm-overlay').cmOverlay();
		});
	</script>
	<!-- //gallery -->
	
	<!-- owl carousel -->
	<script src="<?php bloginfo('template_url');?>/js/owl.carousel.js"></script>
	<script>
		$(document).ready(function () {
			$("#owl-demo").owlCarousel({
				autoPlay: 3000,
				autoPlay: true,
				items: 3,
				itemsDesktop: [991, 2],
				itemsDesktopSmall: [414, 4]
			});

			$("#owl-demo-team").owlCarousel({
				autoPlay: 1500,
				autoPlay: true,
				items: 4,
				itemsDesktop: [991, 2],
				itemsDesktopSmall: [414, 4]
			});

			$("#owl-demo-awards").owlCarousel({
				autoPlay: 1500,
				autoPlay: true,
				items: 4,
				itemsDesktop: [991, 2],
				itemsDesktopSmall: [414, 4]
			});
		});
	</script>
	<!-- //owl carousel -->

	<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/move-top.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/easing.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function ($) {
			$(".scroll").click(function (event) {
				event.preventDefault();

				$('html,body').animate({
					scrollTop: $(this.hash).offset().top
				}, 1000);
			});
		});
	</script>
	<!-- //end-smooth-scrolling -->

	<!-- smooth-scrolling-of-move-up -->
	<script type="text/javascript">
		$(document).ready(function () {
			$().UItoTop({
				easingType: 'easeOutQuart'
			});
		});
	</script>

	<script>
		// Define tamanho das imagens das matérias
		const imagesPosts = [...document.querySelectorAll('.attachment-medium')];
		imagesPosts.forEach(item => {
			item.width = 250;
			item.height = 141;
		});
	</script>

	<script>
		// Define limite de 8 imagens na página inicial
		const imgs = document.querySelectorAll('.modula-item');
		if(imgs.length > 8){
			for(let i = 0; i < imgs.length; i++) {
				if(i >= 8) imgs[i].style.display = 'none';
			}
		}
	</script>

	<script>
		const galeria = [...document.querySelectorAll('.pic')];
		galeria.forEach(image => {
			image.width = 325;
		});
	</script>

	<script>
		for(let i = 1; i <= 3; i++){
			$(`.menu-list-${i}`).hover(
				function(){
					$(`.info-dropdown-${i}`).addClass('color-list');
				},
				function(){
					$(`.info-dropdown-${i}`).removeClass('color-list');
				}
			);
		}
	</script>

	<script src="<?php bloginfo('template_url');?>/js/SmoothScroll.min.js"></script>

	<!-- Bootstrap core JavaScript -->
	<script src="<?php bloginfo('template_url');?>/js/bootstrap.js"></script>
</body>
</html>
# Destino Sustentável

Website do Destino Sustentável.

## Ferramentas utilizadas

* [Wordpress](https://br.wordpress.org/)
* [XAMPP](https://www.apachefriends.org/pt_br/index.html)

## Como Utilizar

* Instale o XAMPP
* Instale o Wordpress na pasta `htdocs` do XAMPP
* Clone este repositório na pasta `themes` do Wordpress
```bash
git clone https://gitlab.com/destinosustentavel/website.git
```
* Inicie o XAMPP e ative o tema no painel do Wordpress

## Como contribuir

* Faça fork deste repositório
* Realize as mudanças no código
* Submeta um merge request
* Aguarde a avaliação da equipe de desenvolvimento

## Licença

[MIT License](https://opensource.org/licenses/MIT)
<div class="header-container">
    <div class="menu-toggle-section">
      <nav class="menu-toogle-nav">
        <ul class="list-menu-toggle">
            <li>
                <a href="#">Início</a>
            </li>
            <li>
                <a class="scroll" href="#about">Sobre</a>
            </li>
            <li>
                <a class="scroll" href="#services">Informações</a>
            </li>
            <li>
                <a class="scroll" href="#blog">Matérias</a>
            </li>
            <li>
                <a class="scroll" href="#team">Equipe</a>
            </li>
            <li>
                <a class="scroll" href="#portfolio">Galeria</a>
            </li>
            <li>
                <a class="scroll" href="#testimonials">+Voluntários</a>
            </li>
            <li>
                <a class="scroll" href="#awards">Prêmios e Títulos</a>
            </li>
            <li>
                <a class="scroll" href="#contact">Contato</a>
            </li>
            <li>
                <a href="https://play.google.com/store/apps/details?id=org.destinosustentavel.dsmovel&hl=pt_BR" target="blank">Aplicativo Móvel</a>
            </li>
            <!-- <li>
                <a href="<?php bloginfo('template_url');?>/login/index.php">Login</a>
            </li>
            <li>
                <a href="<?php bloginfo('template_url');?>/cadastro.php">Cadastro</a>
            </li> -->
            <li>
                <a href="<?php bloginfo('template_url');?>/covid/index.html">COVID-19</a>
            </li>
        </ul>
      </nav>
    </div>
    
    <nav class="menu">
      <a href="#">
        <img src="<?php bloginfo('template_url');?>/images/logo-header.png" alt="Destino Sustentável">
      </a>

      <ul class="list-menu">
        <li>
            <a href="#">Início</a>
        </li>
        <li>
            <a class="scroll" href="#about">Sobre</a>
        </li>
        <li>
            <a class="scroll" href="#services">Informações</a>
        </li>
        <li>
            <a class="scroll" href="#blog">Matérias</a>
        </li>
        <li>
            <a class="scroll" href="#team">Equipe</a>
        </li>
        <li>
            <a class="scroll" href="#portfolio">Galeria</a>
        </li>
        <li>
            <a class="scroll" href="#testimonials">+Voluntários</a>
        </li>
        <li>
            <a class="scroll" href="#awards">Prêmios e Títulos</a>
        </li>
        <li>
            <a class="scroll" href="#contact">Contato</a>
        </li>
        <li>
            <a href="https://play.google.com/store/apps/details?id=org.destinosustentavel.dsmovel&hl=pt_BR" target="blank">Aplicativo Móvel</a>
        </li>
        <!-- <li>
            <a href="<?php bloginfo('template_url');?>/login/index.php">Login</a>
        </li>
        <li>
            <a href="<?php bloginfo('template_url');?>/cadastro.php">Cadastro</a>
        </li> -->
        <li>
            <a href="<?php bloginfo('template_url');?>/covid/index.html">COVID-19</a>
        </li>
      </ul>

      <div class="menu-toggle">
        <span class="one"></span>
        <span class="two"></span>
        <span class="three"></span>
      </div>
    </nav>

    <div class="content">
      <div class="title">
        <h1>Destino<br>Sustentável</h1>
        <p>Reciclar para recriar o futuro.</p>
      </div>

      <img src="<?php bloginfo('template_url');?>/images/hero.png" alt="Reciclar para recriar o futuro">
    </div>
</div>

<script>
    function toggleMenu() {
        const menuToggle = document.querySelector('.menu-toggle');
        const menuToggleSection = document.querySelector('.menu-toggle-section');

        menuToggle.classList.toggle('on');
        menuToggleSection.classList.toggle('on');
    }

    document.addEventListener('DOMContentLoaded', () => {
        const menuToggle = document.querySelector('.menu-toggle');
        menuToggle.addEventListener('click', toggleMenu);

        const menuToggleList = document.querySelector('.list-menu-toggle');
        menuToggleList.querySelectorAll('li').forEach(item => {
            item.addEventListener('click', toggleMenu);
        });
    });
</script>
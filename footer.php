<div class="agileits_w3layouts-map">
    <div class="copyright text-center">
        <h6 class="title-btm about text-center">Contato</h6>
        <ul class="top-links">
            <!--
            <li>
                <span class="fa fa-map-marker" aria-hidden="true"></span>
            </li>
            -->
            <li>
                <div class="ftr-text">
                    <p>Castanhal, Pará, Brasil.</p>
                </div>
            </li>
        </ul>

        <ul class="top-links">
            <!--<li>
                <span class="fa fa-phone" aria-hidden="true"></span>
            </li>
            -->
            <li>
                <div class="ftr-text">
                    <p>+55 (19) 99294 4222</p>
                </div>
            </li>
        </ul>

        <ul class="top-links">
            <!-- <li>
                <span class="fa fa-envelope" aria-hidden="true"></span>
            </li> -->
            <li>
                <div class="ftr-text">
                    <a href="mailto:contato@destinosustentavel.com.br">contato@destinosustentavel.com.br</a>
                </div>
            </li>
        </ul>

        <ul class="top-links">
            <li>
                <a href="https://www.facebook.com/aplicativodestsustentavel" target="blank">
                    <span class="fa fa-facebook"></span>
                </a>
            </li>
            <li>
                <a href="https://twitter.com/sustentaveldest" target="blank">
                    <span class="fa fa-twitter"></span>
                </a>
            </li>
            <li>
                <a href="https://www.instagram.com/destsustentavel/" target="blank">
                    <span class="fa fa-instagram"></span>
                </a>
            </li>
        </ul>
        <p>&copy; 2020 Destino Sustentável. All Rights Reserved.</p>
    </div>
</div>

<!DOCTYPE html>
<html>
<head>
     <script>
        function closeModal(modalId) {
            const modal = document.getElementById(modalId);
            modal.classList.remove('mostrar');
        }
        function openModal(modalId) {
            const modal = document.getElementById(modalId);
            modal.classList.add('mostrar');
        }
    </script>
    <style>
        .popup-container {
         width: 100%;
         height: 100%;
         background: rgba(0,0,0,.5);
         font-family: 'Josefin Sans', sans-serif;
         position: fixed;
         top: 0px;
         left: 0px;
         z-index: 2000;
         display: none;
         justify-content: center;
         align-items: center;
       }

       .mostrar {
         display: flex;
       }

       .popup {
         background: #007b36;
         color: #fff;
         width: 30%;
         min-width: 200px;
         text-align: center;
         padding: 1%;
         border-radius: 7px;
         border: 3px solid white;
       }

       .modal-button {
         text-align: center;
         color: #007b36;
         padding: .5em;
         width: 50%;
         outline: none;
         border: none;
         border-radius: 5px;
         font-size: 14px;
         cursor: pointer;
         letter-spacing: .2em;
         font-weight: bold;
         background: #fff;
       }
    </style>
</head>
<body>

<?php
    include('conexao.php');
    $pessoa = $_POST['radio'];
    session_start();
    if($pessoa == "J"){
	$jtipo_usuario = $_POST["jtipo_user"];
	$nome= $_POST["nome_fantasia"];
	$jcnpj = $_POST["cnpj"];
	$jrepresentante = $_POST["nome_representante"];	
	$jcpf = $_POST["cpf"];
	$jlogradouro = $_POST["logradouro"];
	$jEndereco_Numero = empty($_POST["numero"]) ? 0 : $_POST["numero"];
	$jcidade = $_POST["cidade"];
	$jbairro = $_POST["bairro"];
	$jcomplemento = $_POST["complemento"];
	$jcep = $_POST["cep"];
	$jestado = $_POST['estado'];
	$jtelefone = $_POST["telefone"];
	$jcelular = $_POST["celular"];
	$jemail = $_POST["email"];
	$jemail_secundario = $_POST["email2"];
	$jnomeUser = $_POST["nomeUsuario"];
	$jpassword = md5($_POST["Senha"]);
	$jCpassword = md5($_POST["ConfirmaSenha"]);
	$jimagemPerfil = $_POST["imagem"];
    $tipo_endereco = 'Co'; //comercial
    
	$sql = "SELECT * from usuarios where login='$jnomeUser' or email='$jemail'";
	$busca = $db->query($sql); // verifico se ocorreu resultado
	if($busca->num_rows<1){
		if (filter_var($jemail, FILTER_VALIDATE_EMAIL)) {
			if($jpassword == $jCpassword){
				$db->query("insert into usuarios(nome, login, senha, tipo_pessoa, tipo_usuario, cnpj, representante, email, email_recuperacao, telefone, celular, cpf_representante_jur) values ('$nome','$jnomeUser','$jpassword','$pessoa','$jtipo_usuario','$jcnpj','$jrepresentante','$jemail','$jemail_secundario', '$jtelefone','$jcelular', '$jcpf')") or die("Falha na inserção do usuário banco de dados!");
				$db->query("insert into endereco(usuarios_idUsuario, UF, cidade, logradouro, cep, bairro, complemento, numero_casa, tipo_endereco) values ((SELECT MAX(idUsuario) FROM usuarios),'$jestado','$jcidade','$jlogradouro','$jcep','$jbairro','$jcomplemento','$jEndereco_Numero','$tipo_endereco')") or die("Falha na inserção do endereço no banco de dados!");			    	
										     			   	 
				$verifica = "SELECT idUsuario from usuarios where login='$jnomeUser'"; //retorna uma lista
                $busca = $db->query($verifica);
				$id = $busca->fetch_assoc();	    	
				$_SESSION['Usuario'] = $id['idUsuario']; //seleciono só o que eu quero da lista
				$date = time();
				$verifica = "SELECT login from usuarios where login='$jnomeUser' or email='$jemail'"; //retorna uma lista
                $busca = $db->query($verifica);
				$login = $busca->fetch_assoc();	    	
				$_SESSION['username'] = $login['login'];		
				$_SESSION['password'] = $password;			
				$_SESSION['inicio'] = $date;
				
				$emailsender = "contato@destinosustentavel.org";
                    
                    $headers = "MIME-Version: 1.0\n";
                    $headers .= "Content-type: text/html; charset=iso-8859-1\n";
                    $headers .= "From: Destino Sustentável <contato@destinosustentavel.org>\n"; 
                    $headers .= "Return-Path: $emailsender\n";
                    $headers .= "Reply-To: $jemail\n"; // Endereço (devidamente validado) que o seu usuário informou no contato
                    
                    $subject = "Confirmar conta no Destino Sustentável \n";
                    $mensagem = "Olá <strong>$nome</strong>,
            
                                <br />
            
                                Obrigad@ por se cadastrar no Destino Sustentável<br>
                                Para confirmar sua conta no Destino Sustentável, acesse o link abaixo: <br>
                                <a href ='http://destinosustentavel.org/pos_login/confirma_conta.php?id={$_SESSION['Usuario']}'></a>
                        
                                <br /><br />
                                


                        	
								Quaisquer dúvidas entrem em contato por meio do endereço eletrônico: contato@destinosustentavel.org. <br />
                                Obrigado!<br /> <br /> 

								Destino Sustentável 2017

                                <br /> <br /> <br /> 

                                <b>Esta é uma mensagem automática, por favor não responda!\n";
                    
                    		mail($jemail, $subject, $mensagem, $headers ,"-r".$emailsender);
									
				echo "<META HTTP-EQUIV='REFRESH' CONTENT=\"2;
					URL='www.destinosustentavel.org'\">";
				} else {
                    echo "<script>alert('Senhas não coincidem');</script>";
			        echo "Senhas não coincidem";
				    header("location:cadastro.php");
				}

		} else {
				echo "<script>alert('O email já existe, tente outro!');</script>";
                echo "E-mail inválido";
				/*echo "<META HTTP-EQUIV='REFRESH' CONTENT=\"2;
					URL='cadastro.php'\">";*/
                header("location:cadastro.php");
		}
	} else {
        echo "<script>alert('O email já existe, tente outro!');</script>";
		echo "E-mail já utilizado para outra conta";
        header("location:cadastro.php");
	}					
    } else {
		$tipo_usuario = $_POST['tipo_user'];
		$nome= $_POST['nome'];			
		$nascimento = $_POST['data'];
        //$logradouro = $_POST["logradouro"];
		//$Endereco_Numero = empty($_POST["numero"]) ? 0 : $_POST["numero"];
		//$cidade = $_POST["cidade"];
		//$bairro = $_POST["bairro"];
		//$complemento = $_POST["complemento"];
		$cep = $_POST["cep"];
		//$estado = $_POST['estado'];
		$telefone = $_POST["telefone"];
		$celular = $_POST["celular"];
		$email = $_POST["email"];
		$email_secundario = $_POST["email2"];
		$nomeUser = $_POST["nomeUsuario"];
		$password = md5($_POST["Senha"]);
		$Csenha = md5($_POST["ConfirmaSenha"]);
		$jimagemPerfil = $_POST["imagem"];
		$imagemPerfil = $_POST["imagem"];
        $tipo_endereco = 'Re'; //Residencial
			
        // Conectando com o localhost - mysql
        $sql = "SELECT * from usuarios where login='$nomeUser' or email='$email'";
        $busca = $db->query($sql); // verifico se ocorreu resultado
        if($busca->num_rows<1){
			if (filter_var($email, FILTER_VALIDATE_EMAIL)){
                if($password == $Csenha){
                    /*$db->query("insert into usuarios(nome, data_nascimento, login, senha, tipo_pessoa, tipo_usuario, email, email_recuperacao, telefone, celular)
                    values ('$nome','$nascimento','$nomeUser','$password','$pessoa','$tipo_usuario','$email','$email_secundario', '$telefone','$celular')")
                    or die("Falha ao inserir usuário no banco de dados");					
                    $db->query("insert into endereco(Usuarios_idUsuario, UF, cidade, logradouro, cep, bairro, complemento, numero_casa, tipo_endereco) 
                    values ((SELECT MAX(idUsuario) FROM usuarios),'$estado','$cidade','$logradouro','$cep','$bairro','$complemento','$Endereco_Numero','$tipo_endereco')") 
                    or die("Falha ao inserir endereço no banco de dados!");*/			   

                    $db->query("insert into usuarios(nome, data_nascimento, login, senha, tipo_pessoa, tipo_usuario, email, email_recuperacao, telefone, celular)
                    values ('$nome','$nascimento','$nomeUser','$password','$pessoa','$tipo_usuario','$email','$email_secundario', '$telefone','$celular')")
                    or die("Falha ao inserir usuário no banco de dados");                   
                    $db->query("insert into endereco(Usuarios_idUsuario, cep, tipo_endereco) 
                    values ((SELECT MAX(idUsuario) FROM usuarios),'$cep','$tipo_endereco')") 
                    or die("Falha ao inserir endereço no banco de dados!");

                    $verifica = "SELECT idUsuario from usuarios where login='$nomeUser'"; //retorna uma lista
                    $busca = $db->query($verifica);
				    $id = $busca->fetch_assoc();	    	
				    $_SESSION['Usuario'] = $id['idUsuario']; //seleciono só o que eu quero da lista

				    $date = time();
					$verifica = "SELECT login from usuarios where login='$nomeUser' or email='$email'"; //retorna uma lista
                    $busca = $db->query($verifica);
					$login = $busca->fetch_assoc();	    	
				    $_SESSION['username'] = $login['login'];				
					$_SESSION['password'] = $password;			
					$_SESSION['inicio'] = $date;
					$idU = $_SESSION['Usuario'];
		            $emailsender = "contato@destinosustentavel.org";
                    
                    $headers = "MIME-Version: 1.0\n";
                    $headers .= "Content-type: text/html; charset=iso-8859-1\n";
                    $headers .= "From: Destino Sustentável <contato@destinosustentavel.org>\n"; 
                    $headers .= "Return-Path: $emailsender\n";
                    $headers .= "Reply-To: $email\n"; // Endereço (devidamente validado) que o seu usuário informou no contato
                    
                    $subject = "Confirmar conta no Destino Sustentável \n";
                    $mensagem = "Olá <strong>$nome</strong>,
            
                                <br />
            
                                Obrigad@ por se cadastrar no Destino Sustentável<br>
                                Para confirmar sua conta no Destino Sustentável, acesse o link abaixo: <br>
                                <a href ='http://destinosustentavel.org/confirma_conta.php?id={$idU}'>http://destinosustentavel.org/confirma_conta.php?id={$idU}</a>
                        
                                <br /><br />
                                


                        	
								Quaisquer dúvidas entrem em contato por meio do endereço eletrônico: contato@destinosustentavel.org. <br />
                                Obrigado!<br /> <br /> 

								Destino Sustentável 2017

                                <br /> <br /> <br /> 

                                <b>Esta é uma mensagem automática, por favor não responda!\n";
                    
                    		mail($email, $subject, $mensagem, $headers ,"-r".$emailsender);
                    							
					echo "<META HTTP-EQUIV='REFRESH' CONTENT=\"2;
					URL='www.destinosustentavel.org'\">";
                } else {
                    echo "<script>alert('O email já existe, tente outro!');</script>";
				    echo "Senhas não coincidem";
					/*echo "<META HTTP-EQUIV='REFRESH' CONTENT=\"2;
					URL='tipo_cadastro.php'\">";	*/
                    header("location:cadastro.php");
				}
            } else {
                echo "<script>alert('O email já existe, tente outro!');</script>";
				echo "E-mail inválido";
				/*echo "<META HTTP-EQUIV='REFRESH' CONTENT=\"2;
				URL='cadastro.php'\">";*/
                header("location:cadastro.php");
            }
        
        } else {
            echo "<script>alert('O email já existe, tente outro!');</script>";
            echo "E-mail já utilizado para outra conta";
            /*echo "<META HTTP-EQUIV='REFRESH' CONTENT=\"2;
            URL='cadastro.php'\">";*/
            header("location:cadastro.php");
        }
    }

    // ---------- CÓDIGO DE UPLOAD DA IMAGEM ---------- //
    $id_cadastro = $_SESSION['Usuario'];
    //echo "<h1>$id_cadastro</h1>";
    $target_dir = "imagens_perfil/";
    $name = $_FILES["imagem"]["name"];
    $ext = end((explode(".", $name)));
    $target_file = $target_dir . $id_cadastro . "." . $ext;
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

    // Verifica se o arquivo enviado é uma imagem
    if(isset($_POST["submit"])) {
        $check = getimagesize($_FILES["imagem"]["tmp_name"]);
        if($check !== false) {
            echo "A foto é válida - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            echo "A foto enviada não é válida.";
            $uploadOk = 0;
        }
    }

    // Check if file already exists
    if (file_exists($target_file)) {
        echo "Sorry, file already exists.";
        $uploadOk = 0;
    }

    // Verifica o tamanho da imagem
    if ($_FILES["imagem"]["size"] > 10000000) {
        echo "A foto enviada é muito grande.";
        $uploadOk = 0;
    }

    // Limitar formatos de imagem
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
        //echo "Apenas JPG, JPEG, PNG & GIF são permitidos.";
        $uploadOk = 0;
    }

    //Não deixar enviar arquivos do tipo PHP
    if($imageFileType == "php"){
        echo "Este tipo de arquivo não é permitido!";
    }

    // Verifica se todos os requisitos foram preenchidos
    if($uploadOk == 0) {
        echo "A foto não pode ser enviada.";
    } else {
        if(move_uploaded_file($_FILES["imagem"]["tmp_name"], $target_file)) {
            echo "A foto foi enviada com sucesso.";
        } else {
            echo "Ocorreu um erro ao enviar a foto.";
        }
    }
    
    // ---------- FIM DO CÓDIGO DE UPLOAD DA IMAGEM ---------- //
    echo "Seu Cadastro foi Realizado com sucesso, $nome, obrigado !";
    $db->close();
?>
</body>
</html>
document.addEventListener("DOMContentLoaded", () => {
    // dados do Brasil
    fetch('https://covid19-brazil-api.now.sh/api/report/v1/brazil')
    .then(response => response.json())
    .then(data => {
        const confirmed = data.data.confirmed
        const deaths = data.data.deaths

        document.querySelector('.brazil-confirmed').innerHTML = `${confirmed} Casos`
        document.querySelector('.brazil-deaths').innerHTML = `${deaths} Mortes`
    })

    // dados do Pará
    fetch('https://covid19-brazil-api.now.sh/api/report/v1/brazil/uf/pa')
    .then(response => response.json())
    .then(data => {
        const confirmed = data.cases
        const deaths = data.deaths

        document.querySelector('.para-confirmed').innerHTML = `${confirmed} Casos`
        document.querySelector('.para-deaths').innerHTML = `${deaths} Mortes`
    })
})
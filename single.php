<!DOCTYPE html>
<html lang="zxx">
<head>
	<title>Destino Sustentável</title>
	<!-- for-mobile-apps -->
	<link rel="shortcut icon" href="<?php bloginfo('template_url');?>/images/logo.png">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Reinforce Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<meta property="og:title" content="<?php the_title(); ?>" />
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!-- //for-mobile-apps -->
	<link href="<?php bloginfo('template_url');?>/css/bootstrap.css" rel="stylesheet" media="all" />
	<!--gallery -->
	<link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url');?>/css/cm-overlay.css" />
	<!-- //gallery -->
	<link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/owl.carousel.css" type="text/css" media="all">
	<link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/owl.theme.css" type="text/css" media="all">
	<link href="<?php bloginfo('template_url');?>/css/style1.css" rel="stylesheet" type="text/css" media="all" />
	<!-- Google fonts -->
	<link href="//fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i" rel="stylesheet">
	<!-- //Google fonts -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<link href="<?php bloginfo('template_url');?>/css/font-awesome.css" rel="stylesheet" media="all" />
	<style>
		.posts h2:not(:first-child) {
			width: 850px;
			margin: 10px auto;
		}

		.posts ul li {
			font-size: 17px;
			color: #000;
		}

		.posts ol {
			width: 75%;
			margin: 10px auto;
			padding: 0 15px;
		}

		.posts ol li {
			font-size: 17px;
			margin: 4px 0;
			color: #000;
		}

		.posts p {
			width: 75%;
			margin: 0 auto;
		}
		
		.post-title {
		    text-align: center;
		}

		.rp4wp-related-posts {
			width: 75%;
			margin: 20px auto;
		}

		.rp4wp-related-posts .rp4wp-related-post-content p {
			width: 100%;
		}

		.rp4wp-related-posts ul {
			padding: 0;
		}
	</style>
</head>

<?php
	//include "conexao.php";
	//session_start();
?>

<body>
	<!-- Header -->
	<!-- Slider -->
	<div class="w3-banner-info-agile">
		<div class="slider w3layouts agileits">
			<ul class="rslides w3layouts agileits" id="slider">
				<li>
					<div class="layer agileits-banner  agileits-banner2">
						<p>Reciclar para recriar o futuro</p>
						<h3>D</h3>
						<h3>e</h3>
						<h3>s</h3>
						<h3>t</h3>
						<h3>i</h3>
						<h3>n</h3>
						<h3>o</h3>
						<h3>S</h3>
						<h3>u</h3>
						<h3>s</h3>
						<h3>t</h3>
						<h3>e</h3>
						<h3>n</h3>
						<h3>t</h3>
						<h3>á</h3>
						<h3>v</h3>
						<h3>e</h3>
						<h3>l</h3>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<!-- //Slider -->

	<!-- banner -->
	<div class="banner" id="home">
		<nav class="navbar navbar-default cl-effect-5" id="cl-effect-5">
			<div class="navbar-header navbar-left">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			
			<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li>
						<a href="http://www.destinosustentavel.com.br/" >Início</a>
					</li>
					<!-- <li>
						<a target="_self" href="<?php bloginfo('template_url');?>/login/index.php">Login</a>
					</li>
					<li>
						<a target="_self" href="<?php bloginfo('template_url');?>/cadastro.php">Cadastro</a>
					</li> -->
				</ul>
			</div>
		</nav>
	</div>
	<!-- //banner -->
	<!-- //Header -->

	<!--Matérias-->
	<div class="section-w3ls services-w3ls" id="services1">
		<div class="container">
			<div class="about-main">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<div class="posts">
						<h1 class="post-title"><?php the_title(); ?></h1></br>
						<h5><?php the_subtitle(); ?></h5></br>

						<ul class="share">
							<li>
								<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" style="font-size:32px;color:#3b5998">
									<i class="fab fa-facebook-square"></i>
								</a>
							</li>
							<li>
								<a href="https://api.whatsapp.com/send?text=Voc%c3%aa ainda n%c3%a3o conhece o Destino Sustent%c3%a1vel%3f%0d%0a%0d%0aD%c3%a1 uma olhada nessa mat%c3%a9ria e fique por dentro%3a <?php the_permalink(); ?>" target="_blank"
								style="font-size:32px;color:#25d366">
									<i class="fab fa-whatsapp-square"></i>
								</a>
							</li>
							<li>
								<a href="https://twitter.com/share?url=<?php the_permalink(); ?>&text=Voc%c3%aa+ainda+n%c3%a3o+conhece+o+Destino+Sustent%c3%a1vel%3f%0d%0a%0d%0aD%c3%a1+uma+olhada+nessa+mat%c3%a9ria+e+fique+por+dentro%3a" target="_blank" style="font-size:32px;color:#00aced">
									<i class="fab fa-twitter-square"></i>
								</a>
							</li>
						</ul>

						<h5 class="autor-post">Por <?php the_author_posts_link(); ?></h5>
						<?php the_time('j \d\e F \d\e Y - G:i'); ?>

						<hr/>
						<div class="images-posts">
							<?php if(has_post_thumbnail()): ?>
								<?php the_post_thumbnail('medium_large'); ?>
							<?php endif; ?>
						</div>

						<p><?php the_content(); ?></p>

						<p class="post-tags"><?php the_tags('Tags: '); ?></p>
					</div>
					<?php endwhile?>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<!--//Matérias-->

	<?php include "footer.php" ?>

	<script src="<?php bloginfo('template_url');?>/js/jquery-2.2.3.min.js"></script>
	
	<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/numscroller-1.0.js"></script>
	
	<!-- Slider-JavaScript -->
	<script src="<?php bloginfo('template_url');?>/js/responsiveslides.min.js"></script>
	<script>
		$(function () {
			$("#slider, #slider1").responsiveSlides({
				auto: true,
				nav: false,
				speed: 1500,
				namespace: "callbacks",
				pager: true,
			});
		});
	</script>
	<!-- //Slider-JavaScript -->

	<script>
		$(function () {
			$("#slider3").responsiveSlides({
				auto: true,
				pager: false,
				nav: true,
				speed: 500,
				namespace: "callbacks",
				before: function () {
					$('.events').append("<li>before event fired.</li>");
				},
				after: function () {
					$('.events').append("<li>after event fired.</li>");
				}
			});
		});
	</script>
	<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/numscroller-1.0.js"></script>
	<!-- //gallery -->

	<script src="<?php bloginfo('template_url');?>/js/jquery.tools.min.js"></script>
	<script src="<?php bloginfo('template_url');?>/js/jquery.mobile.custom.min.js"></script>
	<script src="<?php bloginfo('template_url');?>/js/jquery.cm-overlay.js"></script>

	<script>
		$(document).ready(function () {
			$('.cm-overlay').cmOverlay();
		});
	</script>
	<!-- //gallery -->

	<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/move-top.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/easing.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function ($) {
			$(".scroll").click(function (event) {
				event.preventDefault();

				$('html,body').animate({
					scrollTop: $(this.hash).offset().top
				}, 1000);
			});
		});
	</script>
	<!-- //end-smooth-scrolling -->

	<!-- smooth-scrolling-of-move-up -->
	<script type="text/javascript">
		$(document).ready(function () {
			$().UItoTop({
				easingType: 'easeOutQuart'
			});
		});
	</script>

	<script src="<?php bloginfo('template_url');?>/js/SmoothScroll.min.js"></script>

	<!-- Bootstrap core JavaScript -->
	<script src="<?php bloginfo('template_url');?>/js/bootstrap.js"></script>
</body>
</html>
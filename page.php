<!DOCTYPE html>
<html lang="zxx">
<head>
	<title>Destino Sustentável</title>
	<!-- for-mobile-apps -->
	<link rel="shortcut icon" href="<?php bloginfo('template_url');?>/images/icone.ico">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Reinforce Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!-- //for-mobile-apps -->
	<link href="<?php bloginfo('template_url');?>/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<!--gallery -->
	<link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url');?>/css/cm-overlay.css" />
	<!-- //gallery -->
	<link href="<?php bloginfo('template_url');?>/css/font-awesome.css" rel="stylesheet" media="all" />
	<link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/owl.carousel.css" type="text/css" media="all">
	<link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/owl.theme.css" type="text/css" media="all">
	<link href="<?php bloginfo('template_url');?>/css/style1.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?php bloginfo('template_url');?>/css/form.css" rel="stylesheet" type="text/css" media="all" />
	<script src="<?php bloginfo('template_url');?>/js/jquery-2.2.3.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.min.js"></script>
	<!-- Google fonts -->
	<link href="//fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i" rel="stylesheet">
	<!-- //Google fonts -->
	<style>
		.title-agile {
			color: #007b36;
			text-transform: uppercase;
			font-weight: 800;
			font-size: 33px;
		}

		.wp-show-posts-read-more {
			position: absolute;
			bottom: 1%;
			color: #4C484B;
			font-weight: bold;
			font-size: 15px;
			font-family: 'Josefin Sans', sans-serif;
			padding: 0 7%;
		}

		.wp-show-posts-read-more:hover {
			color: #007b36;
		}

		.wp-show-posts-entry-title a {
			color: #000;
		}

		.wp-show-posts-entry-title {
			margin: 20px 0  10px 0;
			font-size: 20px;
			text-transform: capitalize;
			font-weight: 600;
			padding: 0 7%;
		}

		.wp-show-posts-entry-summary p {
			width: 250px;
			font-size: 14px;
			text-align: justify;
			color: rgba(0, 0, 0, 0.5);
			padding: 0 7%;
		}

		.wp-show-posts-single {
			float: left;
			margin-bottom: 2%;
		}

		.wp-show-posts-inner {
			position: relative;
			width: 250px;
			height: 435px;
		}
		.modula-item:hover .description{
			background-color: #007b36;
			color: #ffffff !important;
			transition: .5s;
		}

		.supsystic-tables-wrap {
			visibility: visible !important;
		}

		ul li .supsystic-table tbody tr:first-child {
			font-weight: bold;
			text-transform: uppercase;
		}

		ul li .supsystic-table {
			font-size: 18px;
		}

		.supsystic-table thead {
			text-transform: uppercase;
		}

		ul.top-links {
			display: flex;
			justify-content: center;
			align-items: center;
		}
	</style>
</head>

<body onload="postBox()">
	<!-- Slider -->
	<div class="w3-banner-info-agile">
		<div class="slider w3layouts agileits">
			<ul class="rslides w3layouts agileits" id="slider">
				<li>
					<div class="layer agileits-banner  agileits-banner2">
						<p>Reciclar para recriar o futuro</p>
						<h3>D</h3>
						<h3>e</h3>
						<h3>s</h3>
						<h3>t</h3>
						<h3>i</h3>
						<h3>n</h3>
						<h3>o</h3>
						<h3>S</h3>
						<h3>u</h3>
						<h3>s</h3>
						<h3>t</h3>
						<h3>e</h3>
						<h3>n</h3>
						<h3>t</h3>
						<h3>á</h3>
						<h3>v</h3>
						<h3>e</h3>
						<h3>l</h3>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<!-- //Slider -->

	<!-- banner -->
	<div class="banner" id="home">
		<nav class="navbar navbar-default cl-effect-5" id="cl-effect-5">
			<div class="navbar-header navbar-left">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			
			<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li>
						<a href="http://www.destinosustentavel.com.br/">Início</a>
					</li>
					<!-- <li>
						<a target="_self" href="<?php bloginfo('template_url');?>/login/index.php">Login</a>
					</li>
					<li>
						<a target="_self" href="<?php bloginfo('template_url');?>/cadastro.php">Cadastro</a>
					</li> -->
				</ul>
			</div>
		</nav>
	</div>
	<!-- //banner -->

	<!--Matérias-->
	<div class="section-w3ls services-w3ls" id="materias">
		<h3 class="title-agile about text-center"><?php the_title(); ?></h3>
		<div class="container">
			<div class="about-main">
				<?php
					if ( have_posts() ):
						while ( have_posts() ):
							the_post();
							the_content();
						endwhile;
					else:
						_e( 'Sorry, no posts matched your criteria.', 'devhub' );
					endif;
				?>
			</div>
		</div>
	</div>
	<!--//Matérias-->

	<?php include "footer.php"; ?>

	<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/numscroller-1.0.js"></script>

	<!-- Slider-JavaScript -->
	<script src="<?php bloginfo('template_url');?>/js/responsiveslides.min.js"></script>
	<script>
		$(function () {
			$("#slider, #slider1").responsiveSlides({
				auto: true,
				nav: false,
				speed: 1500,
				namespace: "callbacks",
				pager: true,
			});
		});
	</script>
	<!-- //Slider-JavaScript -->

	<script>
		$(function () {
			$("#slider3").responsiveSlides({
				auto: true,
				pager: false,
				nav: true,
				speed: 500,
				namespace: "callbacks",
				before: function () {
					$('.events').append("<li>before event fired.</li>");
				},
				after: function () {
					$('.events').append("<li>after event fired.</li>");
				}
			});
		});
	</script>

	<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/numscroller-1.0.js"></script>
	<!-- //gallery -->
	
	<script src="<?php bloginfo('template_url');?>/js/jquery.tools.min.js"></script>
	<script src="<?php bloginfo('template_url');?>/js/jquery.mobile.custom.min.js"></script>
	<script src="<?php bloginfo('template_url');?>/js/jquery.cm-overlay.js"></script>

	<script>
		$(document).ready(function () {
			$('.cm-overlay').cmOverlay();
		});
	</script>
	<!-- //gallery -->

	<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/move-top.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/easing.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function ($) {
			$(".scroll").click(function (event) {
				event.preventDefault();

				$('html,body').animate({
					scrollTop: $(this.hash).offset().top
				}, 1000);
			});
		});
	</script>
	<!-- //end-smooth-scrolling -->

	<!-- smooth-scrolling-of-move-up -->
	<script type="text/javascript">
		$(document).ready(function () {
			$().UItoTop({
				easingType: 'easeOutQuart'
			});
		});
	</script>

	<script src="<?php bloginfo('template_url');?>/js/SmoothScroll.min.js"></script>

	<!-- Bootstrap core JavaScript -->
	<script src="<?php bloginfo('template_url');?>/js/bootstrap.js"></script>

	<script>
		function postBox() {
			const posts = [...document.getElementsByClassName("wp-show-posts-inner")];

			posts.forEach(post => {
				post.style.background = "#f5f5f5";
   				post.style.margin = "2%";
   				post.style.boxShadow = "5px 10px 10px #d8d8d8";
			});
		}
	</script>

	<script>
		const images = document.querySelectorAll('.pic');

		images.forEach(image => {
			image.width = 325;
		});
	</script>

	<script>
		const imagesPosts = document.querySelectorAll('.wp-post-image');

		imagesPosts.forEach((image) => {
			image.width = 250;
			image.height = 141;
		});
	</script>

	<script>
		const tabelas = document.querySelectorAll('.supsystic-table');

		tabelas.forEach(tabela => {
			tabela.classList.add('table', 'table-bordered');
			
			const linhas = tabela.querySelectorAll('tbody > tr');
			linhas.forEach((linha, index) => {
				linha.style.backgroundColor = index % 2 === 0 ? '#FFF' : '#E3F2E4';
			});
		});
	</script>
	
	<script>
		$(".pic").on('click', function(){
			let src = $(this).attr("src");
			let str = src.slice(0, -14);
			let str2 = src.slice(-4);
			let result = str + str2;

			const anchor = document.getElementsByClassName('cm-overlay')[0];
			anchor.href = result;
		});
	</script>
</body>
</html>